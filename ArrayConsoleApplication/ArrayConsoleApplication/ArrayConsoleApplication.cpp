﻿#include <iostream>
#include <ctime>
using namespace std;

int main()
{
    const int N = 5;
    int a[N][N];
    for (int i = 0; i < N; i++)
    {
        for (int j = 0; j < N; j++)
        {
            a[i][j] = i + j;
            cout << a[i][j] << ' ';
        }
        cout << endl;
    }
    struct tm buf;
    time_t now = time(NULL);
    localtime_s(&buf, &now);
    int day = buf.tm_mday;

    int sumString = 0;
    for (int j = 0; j < N; j++)
         sumString += a[day % N][j];
    
    cout << "Sum of elements = " << (day % N) << " is " << sumString;

}